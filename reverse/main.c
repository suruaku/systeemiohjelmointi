/*
 * CT30A3370 Käyttöjärjestelmät ja systeemiohjelmointi
 * Author: Ilja Kotirinta
 * Date: 2021-02-01
 * License: do what ever you want
 * Sources:
 * https://www.tutorialspoint.com/
 * man pages
 * https://www.youtube.com/watch?v=5wzmEKjNqiU
 * https://mkyong.com/c/how-to-handle-unknow-size-user-input-in-c/
 * https://gist.github.com/migf1/5559176
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define BUFFER_SIZE 1000

const int STEPSIZE = 100;
int user_input();
char **loadfile(char *filename, int *len);

/* read stdin (user's input), print it reversed to stdout */
int user_input()
{
  unsigned int current_size = 0;

  char *pStr = malloc(STEPSIZE);
  current_size = STEPSIZE;

  if(pStr == NULL) {
    fprintf(stderr, "malloc error");
    exit(1);
  }

  printf("\nGive string to reverse: ");

  int c = EOF;
  unsigned int i = 0;

  while (( c = getchar() ) != '\n') {
    pStr[i++]=(char)c;

    if(i == current_size) {
      current_size = i + STEPSIZE;
      pStr = realloc(pStr, current_size);

      if(pStr == NULL) {
        fprintf(stderr, "realloc error");
        exit(1);
      }
    }
  }

  pStr[i] = '\0';

  for(int l = i - 1; l >= 0; l--){
    printf("%c", pStr[l]);
  }
  printf("\n");

  free(pStr);
  pStr = NULL;

  return 0;
}


/*
 * argunents: filename, length pointer is total length of an array
 * return: reversed content of the file
 */
char **loadfile(char *filename, int *len)
{
  FILE *fp;
  fp = fopen(filename, "r");

  if(fp == NULL) {
    fprintf(stderr, "cannot open file '%s'\n", filename);
    exit(1);
  }

  int arrlen = STEPSIZE;

  // allocate memory for STEPSIZE times char *
  char **lines = (char **)malloc(STEPSIZE * sizeof(char *));
  int i = 0;
  char buf[BUFFER_SIZE];

  if(lines == NULL){
    fprintf(stderr, "malloc failed\n");
    exit(1);
  }

  while (fgets(buf, BUFFER_SIZE, fp)) {

    // extend the array if needed
    if (i == arrlen) {
      arrlen += STEPSIZE;

      char ** newlines = realloc(lines, arrlen * sizeof(char *));
      if (!newlines) {
        fprintf(stderr, "realloc failed\n");
        exit(1);
      }
      lines = newlines;
    }

    // length of a line
    int slen = strlen(buf);

    // allocate memory for the line
    char *str = (char *)malloc((slen + 1) * sizeof(char));
    if(str == NULL){
      fprintf(stderr, "malloc failed\n");
      exit(1);
    }

    // copy from buf to str
    strcpy(str, buf);

    // add str to data structure
    lines[i] = str;

    ++i;
  }

  *len = i;
  return lines;
}

int main( int argc, char **argv)
{
  if (argc == 1) {

    user_input();

  } else if (argc == 2) {

    int length = 0;
    char **words = loadfile(argv[1], &length);

    for (int i = length - 1; i >= 0; i--) {
      printf("%s", words[i]);
    }

  } else if (argc == 3) {

    if( strcmp(argv[1], argv[2]) == 0 ){
      fprintf(stderr, "input and output file must differ\n");
      exit(1);
    };

    int length = 0;
    FILE *fp;
    fp = fopen(argv[2], "w");

    if(fp == NULL) {
      fprintf(stderr, "cannot open file '%s'\n", argv[2]);
      exit(1);
    }

    char **words = loadfile(argv[1], &length);

    for (int i = length - 1; i >= 0; i--) {
      fprintf(fp, "%s", words[i]);
    }

    fclose(fp);

  } else {
    fprintf(stderr, "usage: reverse <input> <output>\n");
    exit(1);
  }

  return 0;
}
